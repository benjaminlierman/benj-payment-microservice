<?php

declare(strict_types=1);

namespace Benj\Payments\AppBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Payum\Core\Model\Payment as BasePayment;

/**
 * @ORM\Entity
 * @ORM\Table
 */
class Payment extends BasePayment
{
}

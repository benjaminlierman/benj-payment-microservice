<?php

declare(strict_types=1);

namespace Benj\Payments\AppBundle\Model;

use Payum\Core\Model\Token;

class PaymentToken extends Token
{
}

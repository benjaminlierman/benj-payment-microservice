<?php

declare(strict_types=1);

namespace Benj\Payments\AppBundle\Event\Subscriber;

use Benj\Payments\AppBundle\Event\AppDomainEvent;
use Benj\Payments\AppBundle\Event\Events;
use Benj\Payments\DomainModel\Event\EventStoreManager;
use Benj\Payments\DomainModel\Event\EventStorePublisher;
use Benj\Payments\Infrastructure\Logging\EventLogger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class DomainEventSubscriber implements EventSubscriberInterface
{
    private $publisher;
    private $manager;
    private $logger;

    public function __construct(
        EventStoreManager $manager,
        EventStorePublisher $publisher,
        EventLogger $logger
    ) {
        $this->publisher = $publisher;
        $this->manager = $manager;
        $this->logger = $logger;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::DOMAIN_EVENT => 'onEvent',
        ];
    }

    public function onEvent(AppDomainEvent $appDomainEvent): void
    {
        // @TODO Write this shit.
    }
}

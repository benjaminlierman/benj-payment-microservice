<?php

declare(strict_types=1);

namespace Benj\Payments\AppBundle\Event;

use Benj\Payments\DomainModel\Event\DomainEvent as DomainEventInterface;
use Symfony\Component\EventDispatcher\Event;

class AppDomainEvent extends Event
{
    private $domainEvent;

    public function __construct(DomainEventInterface $domainEvent)
    {
        $this->domainEvent = $domainEvent;
    }

    public function getDomainEvent(): DomainEventInterface
    {
        return $this->domainEvent;
    }
}

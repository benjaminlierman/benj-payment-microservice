<?php

declare(strict_types=1);

namespace Benj\Payments\AppBundle\Event;

final class Events
{
    public const DOMAIN_EVENT = 'domain.event';
}

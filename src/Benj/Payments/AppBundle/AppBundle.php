<?php

declare(strict_types=1);

namespace Benj\Payments\AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
}

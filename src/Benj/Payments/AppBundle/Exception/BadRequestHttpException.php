<?php

declare(strict_types=1);

namespace Benj\Payments\AppBundle\Exception;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException as BaseBadRequestHttpException;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\ConstraintValidatorInterface;

class BadRequestHttpException extends BaseBadRequestHttpException
{
    public static function becauseValidationFailed(ConstraintValidator $validator)
    {
        $message = 'Validation failed.';

        /** @var ConstraintValidatorInterface $item */
        foreach ($validator as $item) {
            $message .= sprintf(' %s: %s', $item->getPropertyPath(), $item->getItem());
        }
    }

    public static function becauseUnknownDomainEvent(string $className)
    {
        return new static(sprintf('Unknown domain event: %s.', $className));
    }
}

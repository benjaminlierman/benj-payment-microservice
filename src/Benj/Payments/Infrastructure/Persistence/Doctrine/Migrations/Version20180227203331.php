<?php

declare(strict_types=1);

namespace Benj\Payments\Infrastructure\Persistence\Doctrine\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180227203331 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('CREATE TABLE payment_token (id VARCHAR(36) NOT NULL, details VARCHAR(255) NOT NULL, after_url VARCHAR(255) NOT NULL, target_url VARCHAR(255) NOT NULL)');
    }

    public function down(Schema $schema)
    {
        $this->addSql('DROP TABLE payment_token');
    }
}

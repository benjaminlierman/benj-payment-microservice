<?php

declare(strict_types=1);

namespace Benj\Payments\Infrastructure\Persistence\Doctrine;

use Benj\Payments\DomainModel\Payment\Payment;
use Benj\Payments\DomainModel\Payment\PaymentId;
use Benj\Payments\DomainModel\Payment\PaymentRepository as PaymentRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class PaymentRepository extends EntityRepository implements PaymentRepositoryInterface
{
    public function byId(PaymentId $paymentId): Payment
    {
        // TODO: Implement byId() method.
    }
}

<?php

declare(strict_types=1);

namespace Benj\Payments\Infrastructure\Persistence\Doctrine;

use Benj\Payments\DomainModel\Payment\Payment;
use Benj\Payments\DomainModel\Payment\PaymentManager as PaymentManagerInterface;
use Doctrine\ORM\EntityManagerInterface;

class PaymentManager implements PaymentManagerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function create(Payment $payment): void
    {
        $this->entityManager->persist($payment);
        $this->entityManager->flush();
    }

    public function update(Payment $payment): void
    {
        $this->entityManager->merge($payment);
        $this->entityManager->flush();
    }

    public function delete(Payment $payment): void
    {
        $this->entityManager->remove($payment);
        $this->entityManager->flush();
    }
}

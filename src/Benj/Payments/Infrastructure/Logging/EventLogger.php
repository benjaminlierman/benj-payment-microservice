<?php

declare(strict_types=1);

namespace Benj\Payments\Infrastructure\Logging;

use Benj\Payments\DomainModel\Event\StoredEvent;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class EventLogger
{
    private $logger;
    private $serializer;

    public function __construct(LoggerInterface $logger, SerializerInterface $serializer)
    {
        $this->logger = $logger;
        $this->serializer = $serializer;
    }

    public function log(StoredEvent $event): void
    {
        $message = $this->serializer->serialize($event, 'json');
        $this->logger->log(LogLevel::INFO, $message);
    }
}

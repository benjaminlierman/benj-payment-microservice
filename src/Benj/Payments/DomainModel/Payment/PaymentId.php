<?php

declare(strict_types=1);

namespace Benj\Payments\DomainModel\Payment;

class PaymentId
{
    private $id;

    public function __construct(string $anId)
    {
        $this->id = $anId;
        $this->assertNotEmpty();
    }

    public function __toString()
    {
        return $this->id();
    }

    public function id(): string
    {
        return $this->id;
    }

    public function equalsTo(PaymentId $aPaymentId): bool
    {
        return $aPaymentId->id === $this->id;
    }

    private function assertNotEmpty(): void
    {
        if ('' === $this->id) {
            throw new \Exception('Payment id can\'t be empty.');
        }
    }
}

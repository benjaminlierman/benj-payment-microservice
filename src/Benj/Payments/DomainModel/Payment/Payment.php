<?php

declare(strict_types=1);

namespace Benj\Payments\DomainModel\Payment;

class Payment
{
    private $id;

    public function __construct(PaymentId $id)
    {
        $this->id = $id;
    }

    public function setId(PaymentId $id): void
    {
        $this->id = $id;
    }

    public function id(): PaymentId
    {
        return $this->id;
    }
}

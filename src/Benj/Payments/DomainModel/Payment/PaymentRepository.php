<?php

declare(strict_types=1);

namespace Benj\Payments\DomainModel\Payment;

interface PaymentRepository
{
    public function byId(PaymentId $paymentId): Payment;
}

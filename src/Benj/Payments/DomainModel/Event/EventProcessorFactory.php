<?php

declare(strict_types=1);

namespace Benj\Payments\DomainModel\Event;

use Benj\Payments\AppBundle\Exception\BadRequestHttpException;
use Benj\Payments\DomainModel\Payment\PaymentManager;
use Benj\Payments\DomainModel\Payment\PaymentRepository;

class EventProcessorFactory
{
    private $paymentRepository;
    private $paymentManager;

    public function __construct(PaymentRepository $paymentRepository, PaymentManager $paymentManager)
    {
        $this->paymentRepository = $paymentRepository;
        $this->paymentManager = $paymentManager;
    }

    public function create(string $className)
    {
        switch ($className) {
            default:
                throw BadRequestHttpException::becauseUnknownDomainEvent($className);
        }
    }
}

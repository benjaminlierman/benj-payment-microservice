<?php

declare(strict_types=1);

namespace Benj\Payments\DomainModel\Event;

use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class StoredEvent implements DomainEvent
{
    /**
     * @Serializer\Expose
     * @Serializer\Type("string')
     * @Serializer\SerializedName("eventId")
     */
    private $eventId;

    /**
     * @Serializer\Expose
     * @Serializer\Type("string')
     * @Serializer\SerializedName("typedName")
     */
    private $typeName;

    /**
     * @Serializer\Expose
     * @Serializer\Type("string')
     * @Serializer\SerializedName("occuredOn")
     */
    private $occuredOn;

    /**
     * @Serializer\Expose
     * @Serializer\Type("string')
     * @Serializer\SerializedName("eventBody")
     */
    private $eventBody;

    public function __construct(string $eventId, string $typeName, \DateTimeImmutable $occuredOn, string $eventBody)
    {
        $this->eventId = $eventId;
        $this->typeName = $typeName;
        $this->occuredOn = $occuredOn;
        $this->eventBody = $eventBody;
    }

    public function eventId(): string
    {
        return $this->eventId;
    }

    public function typeName(): string
    {
        return $this->typeName;
    }

    public function occuredOn(): \DateTimeImmutable
    {
        return $this->occuredOn;
    }

    public function eventBody(): string
    {
        return $this->eventBody;
    }
}

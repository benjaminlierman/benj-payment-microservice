<?php

declare(strict_types=1);

namespace Benj\Payments\DomainModel\Event;

interface EventStoreRepository
{
    public function allSince(int $eventId): array;
}

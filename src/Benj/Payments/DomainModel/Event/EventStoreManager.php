<?php

declare(strict_types=1);

namespace Benj\Payments\DomainModel\Event;

interface EventStoreManager
{
    public function append(StoredEvent $event): void;
}

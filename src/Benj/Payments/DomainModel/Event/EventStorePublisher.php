<?php

declare(strict_types=1);

namespace Benj\Payments\DomainModel\Event;

interface EventStorePublisher
{
    public function publish(StoredEvent $event): void;
}

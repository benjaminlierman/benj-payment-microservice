<?php

declare(strict_types=1);

namespace Benj\Payments\DomainModel\Event;

interface DomainEvent
{
    public function occuredOn(): \DateTimeImmutable;
}

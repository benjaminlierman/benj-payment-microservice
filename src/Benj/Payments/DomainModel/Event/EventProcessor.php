<?php

declare(strict_types=1);

namespace Benj\Payments\DomainModel\Event;

interface EventProcessor
{
    public function process(DomainEvent $domainEvent): void;
}

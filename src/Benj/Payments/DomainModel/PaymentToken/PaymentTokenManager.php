<?php

declare(strict_types=1);

namespace Benj\Payments\DomainModel\Payment;

interface PaymentTokenManager
{
    public function create(Payment $payment): void;

    public function update(Payment $payment): void;

    public function delete(Payment $payment): void;
}

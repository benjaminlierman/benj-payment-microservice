<?php

declare(strict_types=1);

namespace Benj\Payments\DomainModel\PaymentToken;

class PaymentToken
{
    private $id;

    public function __construct(PaymentTokenId $id)
    {
        $this->id = $id;
    }

    public function setId(PaymentTokenId $id): void
    {
        $this->id = $id;
    }

    public function id(): PaymentTokenId
    {
        return $this->id;
    }
}

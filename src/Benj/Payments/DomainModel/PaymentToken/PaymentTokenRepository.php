<?php

declare(strict_types=1);

namespace Benj\Payments\DomainModel\Payment;

interface PaymentTokenRepository
{
    public function byId(PaymentId $paymentId): Payment;
}
